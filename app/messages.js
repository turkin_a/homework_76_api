const express = require('express');
const router = express.Router();

const createRouter = db => {
  router.get('/', (req, res) => {
    res.send(db.getData());
  });

  router.post('/', (req, res) => {
    if (req.body.author === '' || req.body.message === '')
      res.status(400).send({error: 'Author and message must be present in the request'});
    else {
      db.addMessage(req.body).then(result => {
        res.send(result);
      });
    }
  });

  router.get('/?datetime', (req, res) => {
    res.send(db.getDataByDate(req.params.datetime));
  });

  return router;
};

module.exports = createRouter;