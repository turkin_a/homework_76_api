const express = require('express');
const app = express();
const fs = require('fs');
const nanoid = require('nanoid');
const cors = require('cors');
const port = 8000;

app.use(express.json());
app.use(cors());

let data = [];

const getData = () => {
  return new Promise((resolve, reject) => {
    fs.readFile('./dataBase.json', (err, content) => {
      if (err) reject(err);
      resolve(content);
    })
  })
};

getData().then((result) => {
  data = JSON.parse(result);
  console.log(data);
}).catch(err => {
  console.log(err);
});

app.post('/messages', (req, res) => {
  if (req.body.name === '' || req.body.message === '') {
    res.send('Please fill all blanks')
  } else {
      let content = req.body;
      const dateTime = new Date().toISOString();
      const path = `./dataBase.json`;
      content.id = nanoid();
      content.date = dateTime;
      data.push(content);

      fs.writeFile(path, JSON.stringify(data), error => {
        if (error) throw error;
        res.send('Saved')
  })}
});

app.get('/messages', (req, res) => {
  res.send(data.slice(-30));
});

app.listen(port, () => {
  console.log(`Server started on ${port} port`);
});

